package main

import (
	"net/http"
	"os"

	"gifs.club/code/api"
	"gifs.club/code/metadata/storers"
	"gifs.club/code/storage"

	yall "yall.in"
	"yall.in/colour"
)

func main() {
	log := yall.New(colour.New(os.Stdout, yall.Debug))

	metadataStorer, err := storers.NewMemory()
	if err != nil {
		log.WithError(err).Error("error creating new in-memory metadata storer")
		os.Exit(1)
	}
	blobStorer, err := storage.NewMemstore()
	if err != nil {
		log.WithError(err).Error("error creating new in-memory blob storer")
		os.Exit(1)
	}
	srv := api.Service{
		Log:         log,
		Storage:     blobStorer,
		GIFs:        metadataStorer,
		Collections: metadataStorer,
	}
	http.Handle("/v1/", srv.Server("/v1"))
	err = http.ListenAndServe(":12345", nil)
	if err != nil {
		log.WithError(err).Error("error starting server")
		os.Exit(1)
	}
}
